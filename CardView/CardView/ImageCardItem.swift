//
//  ImageCardItem.swift
//  ImageCardItem
//
//  Created by Anshul
//

import UIKit

class ImageCardItem: CardItem {

    var image: UIImage
    var imageView: UIImageView!
    init(image: UIImage) {
        self.image = image
        super.init(frame: CGRect.zero)
        initView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func initView() {
        imageView = UIImageView()
        imageView.image = image
       contentView.addSubview(imageView)
    }
    
    override func layoutSubviews() {
         super.layoutSubviews()
        imageView.frame = bounds
    }
    
}
